
#include <stdio.h>
#include <stdlib.h>


int main() {
    
    int filas, columnas, fila, columna;
    printf("Ingrese las filas de la matriz\n");
    scanf("%d", &filas);
    printf("Ingrese las columnas de la matriz\n");
    scanf("%d", &columnas);
    
    int A[filas][columnas];
    int B[filas][columnas];
    int C[filas][columnas];
    
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            
            printf("ingrese valores de la matriz A %d%d \n", fila+1, columna+1);
            scanf("%d", &A[fila][columna]);
        }
    }
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            
            printf("ingrese valores de la matriz B %d%d \n", fila+1, columna+1);
            scanf("%d", &B[fila][columna]);
        }
    }
    
    //Multiplicacion de ambas matrices
    for(fila=0; fila<filas; fila++){
        for(columna=0; columna<columnas; columna++){
            C[fila][columna]=A[fila][columna]*B[columna][fila];
            
        }
    }
    
    printf("El producto es :\n");
    for(fila=0; fila<filas; fila++){

        for(columna=0; columna<columnas; columna++){
            printf("[%d]",C[fila][columna] );   
        }
        printf("\n");   
    }

    return 0;
}

