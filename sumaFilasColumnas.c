#include <stdio.h>
#include <stdlib.h>

int main() {

    int filas, columnas, fila, columna, suma, contador, sd1 = 0, sd2 = 0;
    int vertical[filas], horizontal[columnas];
    printf("Ingrese las filas de la matriz \n");
    scanf("%d", &filas);
    printf("Ingrese las columnas de la matriz \n");
    scanf("%d", &columnas);
    int matriz[filas][columnas];

    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {

            printf("ingrese valores de la matriz %d%d \n", fila + 1, columna + 1);
            scanf("%d", &matriz[fila][columna]);
        }
    }
    printf("\n\nvalores de la matriz:\n");
    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            printf("[%d]", matriz[fila][columna]);
        }
        printf("\n");

    }

    for (fila = 0; fila < filas; fila++) {
        suma = 0;
        for (columna = 0; columna < columnas; columna++) {
            suma += matriz[fila][columna];
        }
        horizontal[fila] = suma;
    }

    printf("la suma de cada fila es: \n");
    int fil = 0;
    for (contador = 0; contador < filas; contador++) {
        fil = fil + 1;
        printf("fila#%d=\t%d\t", fil, horizontal[contador]);
    }

    for (columna = 0; columna < columnas; columna++) {
        suma = 0;
        for (fila = 0; fila < filas; fila++) {
            suma += matriz[fila][columna];
        }
        vertical[columna] = suma;
    }

    printf("\nla suma de cada columna es: \n");
    int col = 0;
    for (contador = 0; contador < columnas; contador++) {
        col = col + 1;
        printf("columna#%d=\t%d\t",col, vertical[contador]);
    }


    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            if (fila == columna)
                sd1 += matriz[fila][columna];
        }
    }

    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            if (fila + columna == filas - 1)
                sd2 += matriz[fila][columna];
        }
    }


    printf("\n\nSuma de los elementos de la diagonal principal es %d", sd1);
    printf("\n\nSuma de los elementos de la diagonal secundaria es %d\n", sd2);


    return 0;
}

